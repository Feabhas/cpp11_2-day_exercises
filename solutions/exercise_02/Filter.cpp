#include "Filter.h"
#include <iostream>

using namespace std;

void Generator::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Generator::execute:" << endl;

  int nr = (rand() % 3);
 
  switch(nr)
  {
  case 0:
    output->push(Event{Event::Type::WARNING });
    break;

  case 1:
    output->push(Event{Event::Type::CAUTION });
    break;

  case 2:
    output->push(Event{Event::Type::ADVISORY });
    break;
  }
}


void Display::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Display::execute:" << endl;

  Event event{};
  input->pull(event);

  cout << event.typeAsString() << endl;
}
