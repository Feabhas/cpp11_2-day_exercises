#ifndef PIPE_H
#define PIPE_H

#include <stdint.h>
#include <stdexcept>
#include <array>
#include <mutex>
#include <condition_variable>

using namespace std;

// ---------------------------------------------------------------------------
// Pipe exception classes, derived from
// std::out_of_range.
//
class pipe_empty : public out_of_range
{
public:
  pipe_empty() : out_of_range{ "Pipe empty!" }    {}
  pipe_empty(const char* str) : out_of_range{ str } {}
};


class pipe_full : public out_of_range
{
public:
  pipe_full() : out_of_range{ "Pipe full!" }     {}
  pipe_full(const char* str) : out_of_range{ str } {}
};


// ---------------------------------------------------------------------------
// The Pipe class acts as a generic First-In-First-Out
// message queue.
//
template <typename T = int, uint32_t sz = 8>
class Pipe
{
public:
  Pipe()                       = default;
  ~Pipe()                      = default;
  Pipe(const Pipe&)            = delete;
  Pipe& operator=(const Pipe&) = delete;

  // Meyers' 'Universal reference' template
  // overload for l-value and r-value references
  //
  template<typename U>
  void push(U&& in_val);

  void pull(T& inout_val);
  bool isEmpty();
  uint32_t size();

private:
  array<T, sz> buffer;
  typename array<T, sz>::iterator read  = buffer.begin();
  typename array<T, sz>::iterator write = buffer.begin();
  unsigned numItems = 0;
};


template <typename T, uint32_t sz>
template <typename U>
void Pipe<T, sz>::push(U&& in_val)
{
  if (numItems == sz) throw pipe_full();

  // Will move for r-value objects; copy
  // for l-value objects
  //
  *write = move(in_val);
  ++numItems;
  ++write;
  if (write == buffer.end()) write = buffer.begin();
}


template <typename T, uint32_t sz>
void Pipe<T, sz>::pull(T& inout_val)
{
  if(numItems == 0) throw pipe_empty();

  // Move the element out of the buffer. If
  // move is not supported by the element then
  // copy
  //
  inout_val = move(*read);
  --numItems;
  ++read;
  if(read == buffer.end()) read = buffer.begin();
}


template <typename T, uint32_t sz>
bool  Pipe<T, sz>::isEmpty()
{
  return (numItems == 0);
}


template <typename T, uint32_t sz>
uint32_t Pipe<T, sz>::size()
{
  return numItems;
}


// -----------------------------------------------------------------------
//
template <typename T = int, uint32_t sz = 8>
class ThreadSafePipe : Pipe<T, sz>
{
public:
  ThreadSafePipe() = default;
  ~ThreadSafePipe() = default;
  ThreadSafePipe(const ThreadSafePipe&) = delete;
  ThreadSafePipe& operator=(const ThreadSafePipe&) = delete;

  template<typename U>
  void push(U&& in_val);

  void pull(T& inout_val);
  bool isEmpty();

private:
  mutex mtx;
  condition_variable bufferHasSpace;
  condition_variable bufferNotEmpty;
};


template <typename T, uint32_t sz>
template <typename U>
void ThreadSafePipe<T, sz>::push(U&& in_val)
{
  unique_lock<mutex> lock(mtx);
  while(Pipe<T, sz>::size() == sz)
  {
    bufferHasSpace.wait(lock);
  }

  Pipe<T, sz>::push(forward<U>(in_val));
  bufferNotEmpty.notify_all();
}


template <typename T, uint32_t sz>
void ThreadSafePipe<T, sz>::pull(T& inout_val)
{
  unique_lock<mutex> lock(mtx);
  while(Pipe<T, sz>::isEmpty())
  {
    bufferNotEmpty.wait(lock);
  }

  Pipe<T, sz>::pull(inout_val);
  bufferHasSpace.notify_all();
}


template <typename T, uint32_t sz>
bool ThreadSafePipe<T, sz>::isEmpty()
{
  lock_guard<mutex> scopeLock(mtx);
  return Pipe<T,sz>::isEmpty();
}


#endif // PIPE_H
