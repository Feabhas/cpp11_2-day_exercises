#ifndef EVENTLIST_H
#define EVENTLIST_H

#include "Event.h"
#include <vector>

class EventList : private std::vector<Event>
{
public:
  EventList();

  using std::vector<Event>::emplace_back;
  using std::vector<Event>::push_back;
  using std::vector<Event>::begin;
  using std::vector<Event>::end;
  using std::vector<Event>::size;
  using std::vector<Event>::value_type;
};


#endif // EVENTLIST_H
