#include "Event.h"
#include "Pipe.h"
#include "Pipeline.h"
#include "Filter.h"

using namespace std;

int main()
{
  Pipe_Ty   pipe1{};
  Pipe_Ty   pipe2{};

  Generator generator{ pipe1 };
  IDFilter  filter{ pipe1, pipe2, Event::Type::WARNING };
  Display   display{ pipe2 };

  Pipeline filterchain;

  filterchain.add(generator);
  filterchain.add(filter);
  filterchain.add(display);
  filterchain.run();
}
