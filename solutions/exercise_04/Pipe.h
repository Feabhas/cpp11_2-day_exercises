#ifndef PIPE_H
#define PIPE_H

#include <stdint.h>
#include <stdexcept>
#include <array>

using namespace std;

// ---------------------------------------------------------------------------
// Pipe exception classes, derived from
// std::out_of_range.
//
class pipe_empty : public out_of_range
{
public:
  pipe_empty() : out_of_range{ "Pipe empty!" }    {}
  pipe_empty(const char* str) : out_of_range{ str } {}
};


class pipe_full : public out_of_range
{
public:
  pipe_full() : out_of_range{ "Pipe full!" }     {}
  pipe_full(const char* str) : out_of_range{ str } {}
};


// ---------------------------------------------------------------------------
// The Pipe class acts as a generic First-In-First-Out
// message queue.
//
template <typename T = int, uint32_t sz = 8>
class Pipe
{
public:
  Pipe()                       = default;
  ~Pipe()                      = default;
  Pipe(const Pipe&)            = delete;
  Pipe& operator=(const Pipe&) = delete;

  void push(const T& in_val);
  void pull(T& inout_val);
  bool isEmpty();

private:
  array<T, sz> buffer;
  typename array<T, sz>::iterator read  = buffer.begin();
  typename array<T, sz>::iterator write = buffer.begin();
  unsigned numItems = 0;
};


template <typename T, uint32_t sz>
void Pipe<T, sz>::push(const T& in_val)
{
  if (numItems == sz) throw pipe_full();

  *write = in_val; // Copy
  ++numItems;
  ++write;
  if (write == buffer.end()) write = buffer.begin();
}


template <typename T, uint32_t sz>
void Pipe<T, sz>::pull(T& inout_val)
{
  if(numItems == 0) throw pipe_empty();

  inout_val = *read;
  --numItems;
  ++read;
  if(read == buffer.end()) read = buffer.begin();
}


template <typename T, uint32_t sz>
bool  Pipe<T, sz>::isEmpty()
{
  return (numItems == 0);
}


#endif // PIPE_H
