#include "Filter.h"
#include <iostream>

using namespace std;

void Generator::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Generator::execute:" << endl;

  // Generate a random number of Events and add them
  // to an EventList
  //
  int numEvents = (rand() % 3) + 3;
  cout << "Generating " << numEvents << " events" << endl;
  EventList events;
  cout << endl;

  for(int i = 0; i < numEvents; ++i)  // Create at least three Events.
  {
    int eventType = rand() % 3;
    switch(eventType)
    {
    case 0:
      events.emplace_back(Event::Type::WARNING);
      break;

    case 1:
      events.emplace_back(Event::Type::CAUTION);
      break;

    case 2:
      events.emplace_back(Event::Type::ADVISORY);
      break;
    }
  }
  output->push(events);
}


void Display::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Display::execute:" << endl;

  EventList events{};
  input->pull(events);

  for(auto& event : events)
  {
    cout << event.typeAsString() << endl;
  }
}
