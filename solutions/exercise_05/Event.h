#ifndef EVENT_H
#define EVENT_H

#include <cstddef>
using std::size_t;

// ---------------------------------------------------------------------------
// Events represent alarm conditions in the system.
//
class Event
{
public:
  enum class Type { WARNING, CAUTION, ADVISORY, UNKNOWN };

  Event() = default;
  Event(Type t);
  Event(Type t, const char* str);

  // Copy and move policy
  //
  ~Event();
  Event(const Event& rhs);
  Event(Event&& rhs) noexcept;
  Event& operator=(Event rhs);

  const char* typeAsString() const;
  Event::Type type() const;
  const char* what() const;

private:
  Type eventType     = Type::UNKNOWN;
  size_t sz          = 0;
  char*  description = nullptr;

  friend void swap(Event& lhs, Event& rhs) noexcept;
};

#endif // EVENT_H

