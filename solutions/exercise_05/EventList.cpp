#include "EventList.h"

EventList::EventList() : 
std::vector<Event>{}
{
  // There will be at least three
  // elements in the EventList, so
  // pre-allocate memory for them; and
  // grow if necessary
  //
  std::vector<Event>::reserve(3);
}