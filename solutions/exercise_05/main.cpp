#include "Event.h"
#include "Pipe.h"
#include "Pipeline.h"
#include "Filter.h"

using namespace std;

int main()
{
  Pipe_Ty   pipe{};
  Generator generator{ pipe };
  Display   display{ pipe };
  Pipeline  filterchain{};

  filterchain.add(generator);
  filterchain.add(display);
  filterchain.run();
}
