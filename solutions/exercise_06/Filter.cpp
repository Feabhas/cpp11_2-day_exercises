#include "Filter.h"
#include "EventList.h"
#include <iostream>
#include <memory>

using namespace std;

void Generator::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Generator::execute" << endl;
  int numEvents = (rand() % 3) + 3;

  // Generate a random number of Events and add them
  // to an EventList
  //
  cout << "Generating " << numEvents << " events" << endl;
  auto events = make_shared<EventList>();
  cout << endl;

  for(int i = 0; i < numEvents; ++i)  // Create at least three Events.
  {
    int eventType = rand() % 3;
    switch(eventType)
    {
    case 0:
      events->emplace_back(Event::Type::WARNING, "This is a warning");
      break;

    case 1:
      events->emplace_back(Event::Type::CAUTION, "Be careful");
      break;

    case 2:
      events->emplace_back(Event::Type::ADVISORY, "You should fix this soon");
      break;
    }
  }

  output->push(move(events));
}


void Display::execute()
{
  cout << "-----------------------------" << endl;
  cout << "Display::execute:" << endl;

  auto events = make_shared<EventList>();
  input->pull(events);

  for(auto& event : *events)
  {
    cout << event.typeAsString() << ": " << event.what() << endl;
  }
}
