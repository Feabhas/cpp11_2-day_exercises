#include "Event.h"
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

Event::Event(Event::Type t) : 
  eventType{ t },
  sz{ 0 },
  description{ nullptr }
{
}


Event::Event(Event::Type t, const char* str) :
  eventType{ t },
  sz{ strlen(str) + 1 },
  description{ new char[sz] }
{
  strcpy(description, str);
}


namespace
{
  const char* eventTypeStrings[] =
  {
    "WARNING",
    "CAUTION",
    "ADVISORY",
    "UNKNOWN"
  };


  const char* asString(Event::Type type)
  {
    return eventTypeStrings[static_cast<unsigned>(type)];
  }
}


const char* Event::typeAsString() const
{
  return asString(eventType);
}


Event::Type Event::type() const
{
  return eventType;
}


const char* Event::what() const
{
  if(description != nullptr) return description;
  else return "";
}


Event::~Event()
{
  delete[] description;
}


Event::Event(const Event& rhs) :
  eventType{rhs.eventType},
  sz{ rhs.sz },
  description{rhs.description}
{
  if(rhs.description != nullptr)
  {
    description = new char[rhs.sz];
    strcpy(description, rhs.description);
  }
}


Event::Event(Event&& rhs) noexcept :
  Event()
{
  swap(*this, rhs);
}


Event& Event::operator=(Event rhs)
{
  swap(*this, rhs);
  return *this;
}


void swap(Event& lhs, Event& rhs) noexcept
{
  std::swap(lhs.eventType, rhs.eventType);
  std::swap(lhs.sz, rhs.sz);
  std::swap(lhs.description, rhs.description);
}
