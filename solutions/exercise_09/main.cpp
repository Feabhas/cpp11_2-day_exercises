#include "Event.h"
#include "Pipe.h"
#include "Filter.h"
#include <thread>

using namespace std;

int main()
{
  Pipe_Ty   pipe1{};
  Pipe_Ty   pipe2{};

  Generator generator{ pipe1 };
  IDFilter  filter{ pipe1, pipe2, Event::Type::WARNING };
  Display   display{ pipe2 };

  thread generatorThread{ &IFilter::execute, &generator };
  thread filterThread{ &IFilter::execute, &filter };
  thread displayThread{ &IFilter::execute, &display };

  generatorThread.join();
  filterThread.join();
  displayThread.join();
}
