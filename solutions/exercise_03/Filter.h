#ifndef FILTER_H
#define FILTER_H

#include "Pipe.h"
#include "EventList.h"

// ---------------------------------------------------------------------------
// Filter interface
//
class IFilter
{
public:
	virtual void execute() = 0;
	virtual ~IFilter()     = default;
};

// Type alias for template Pipe
//
using Pipe_Ty = Pipe<EventList, 16>;


// ---------------------------------------------------------------------------
// Generator creates Events (randomly).  It does not receive
// any inputs; thus only has an output Pipe.
//
class Generator : public IFilter
{
public:
  Generator(Pipe_Ty& out) : output{ &out } {}
	virtual void execute() override;

private:
	Pipe_Ty* output;
};

// ---------------------------------------------------------------------------
// A Display class displays the Events it receives.  It does not
// have any outputs, therefore is simply a Sink-type Filter.
//
class Display : public IFilter
{
public:
  Display(Pipe_Ty& in) : input{ &in } {}
	virtual void execute() override;

private:
	Pipe_Ty* input;
};



#endif // FILTER_H
