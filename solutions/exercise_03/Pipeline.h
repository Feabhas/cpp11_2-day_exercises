#ifndef PIPELINE_H
#define PIPELINE_H
#include <vector>

using namespace std;

class IFilter;

// ---------------------------------------------------------------------------
// A Pipeline encapsulates a list of Filter objects
// and runs through them, in sequence.
//
class Pipeline
{
public:
  Pipeline();
  void add(IFilter& filter);
  void run();

private:
  vector<IFilter*> filters;
};



#endif //PIPELINE_H
