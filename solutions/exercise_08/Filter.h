#ifndef FILTER_H
#define FILTER_H

#include "Pipe.h"
#include "EventList.h"
#include <memory>

// ---------------------------------------------------------------------------
// Filter interface
//
class IFilter
{
public:
	virtual void execute() = 0;
	virtual ~IFilter()     = default;
};

// Type alias for template Pipe
//
using Pipe_Ty = Pipe<std::shared_ptr<EventList>, 16>;


// ---------------------------------------------------------------------------
// Generator creates Events (randomly).  It does not receive
// any inputs; thus only has an output Pipe.
//
class Generator : public IFilter
{
public:
  Generator(Pipe_Ty& out) : output{ &out } {}
	virtual void execute() override;

private:
	Pipe_Ty* output;
};


// ---------------------------------------------------------------------------
// A Display class displays the Events it receives.  It does not
// have any outputs, therefore is simply a Sink-type Filter.
//
class Display : public IFilter
{
public:
  Display(Pipe_Ty& in) : input{ &in } {}
	virtual void execute() override;

private:
	Pipe_Ty* input;
};


// ---------------------------------------------------------------------------
// IDFilter removes Events from an EventList, if the ID matches the
// filter value.
// This filter sits in the middle of a filter-chain, so has both an
// input Pipe and output Pipe.
//
class IDFilter : public IFilter
{
public:
  IDFilter(Pipe_Ty& in, Pipe_Ty& out, Event::Type filterValue);
  virtual void execute();

private:
  Pipe_Ty* input;
  Pipe_Ty* output;
  Event::Type filterValue;
};

#endif // FILTER_H
