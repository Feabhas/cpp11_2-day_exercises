#include "Filter.h"
#include "EventList.h"
#include <iostream>
#include <memory>
#include <algorithm>
#include <thread>
#include <chrono>

using namespace std;

void Generator::execute()
{
  while(true) // MAIN CONTROL LOOP
  {
    cout << "-----------------------------" << endl;
    cout << "Generator::execute" << endl;
    int numEvents = (rand() % 3) + 3;

    // Generate a random number of Events and add them
    // to an EventList
    //
    cout << "Generating " << numEvents << " events" << endl;
    auto events = make_shared<EventList>();
    cout << endl;

    for(int i = 0; i < numEvents; ++i)  // Create at least three Events.
    {
      int eventType = rand() % 3;
      switch(eventType)
      {
      case 0:
        events->emplace_back(Event::Type::WARNING, "This is a warning");
        break;

      case 1:
        events->emplace_back(Event::Type::CAUTION, "Be careful");
        break;

      case 2:
        events->emplace_back(Event::Type::ADVISORY, "You should fix this soon");
        break;
      }
    }
    output->push(move(events));
    this_thread::sleep_for(chrono::milliseconds(2000));

  } // END OF MAIN CONTROL LOOP
}


void Display::execute()
{
  while(true) // MAIN CONTROL LOOP
  {
    // Spin while the pipe is empty
    //
    if(input->isEmpty()) continue;

    cout << "-----------------------------" << endl;
    cout << "Display::execute:" << endl;

    auto events = make_shared<EventList>();
    input->pull(events);

    for(auto& event : *events)
    {
      cout << event.typeAsString() << ": " << event.what() << endl;
    }
  } // END OF MAIN CONTROL LOOP
}


IDFilter::IDFilter(Pipe_Ty& in, Pipe_Ty& out, Event::Type ID) :
  input{ &in },
  output{ &out },
  filterValue{ ID }
{
}


void IDFilter::execute()
{
  while(true)  // MAIN CONTROL LOOP
  {
    // Spin while the pipe is empty
    //
    if(input->isEmpty()) continue;

    cout << "-----------------------------" << endl;
    cout << "IDFilter::execute" << endl;

    auto events = make_shared<EventList>();
    auto filteredEvents = make_shared<EventList>();
    input->pull(events);

    cout << "Input EventList has " << events->size() << " events" << endl;

    // Using move_iterators to force move of Events
    // instead of copy (of course, this makes the algorithm
    // name rather confusing!)
    //
    copy_if(make_move_iterator(events->begin()),
            make_move_iterator(events->end()),
            back_inserter(*filteredEvents),
            [this](const Event& e) { return e.type() == filterValue; });

    cout << "Output EventList has " << filteredEvents->size() << " events" << endl;

    output->push(move(filteredEvents));

  }  // END OF MAIN CONTROL LOOP
}
