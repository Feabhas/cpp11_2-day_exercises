#include "Event.h"
#include "Pipe.h"
#include <iostream>

using namespace std;

int main()
{
  Event e1{ };
  Event e2{ Event::Type::ADVISORY };
  
  cout << e1.typeAsString() << endl;
  cout << e2.typeAsString() << endl;
}
