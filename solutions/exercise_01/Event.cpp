#include "Event.h"
#include <string>
#include <iostream>

using namespace std;

Event::Event() : 
   eventType{ Event::Type::UNKNOWN } 
{
}


Event::Event(Event::Type t) : 
  eventType{ t } 
{
}


namespace
{
  const char* eventTypeStrings[] =
  {
    "WARNING",
    "CAUTION",
    "ADVISORY",
    "UNKNOWN"
  };

  const char* asString(Event::Type type)
  {
    return eventTypeStrings[static_cast<unsigned>(type)];
  }
}


const char* Event::typeAsString() const
{
  return asString(eventType);
}


Event::Type Event::type() const
{
  return eventType;
}
