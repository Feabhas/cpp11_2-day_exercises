#ifndef FILTER_H
#define FILTER_H

#include "Pipe.h"

// ---------------------------------------------------------------------------
// Filter interface
//
class IFilter
{
public:
	virtual void execute() = 0;
};

/*************************************************************************
  Create a Generator class that realises I_Filter and, when run, creates a
  (random) Event and pushes it onto its output Pipe.

  Create a Display class that realises I_Filter and, when run, pulls an 
  Event from its input Pipe and displays its description to the screen.

*************************************************************************/


#endif // FILTER_H
