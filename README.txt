======================================================================
INTRODUCTION TO MODERN C++ (2-day variant)
======================================================================

This repo contains the code templates and solutions
for the two-day variant of the Feabhas Modern C++
courses.

======================================================================
WINDOWS SET-UP
--------------

A Visual Studio solution for VS2012 onwards has already
been created and configured.

Open the VS2012_solution.sln file with Visual Studio. You
may be required to convert the project to a newer configuration
if you are using a more recent version of Visual Studio.

Rebuild the project and ensure there are no errors

======================================================================
ECLIPSE SET-UP
--------------

Create a new Empty C++ project using the Eclipse project wizard.

Configure the project as follows (you may do this during creation
of the project, or afterwards)

Set the C++ dialect to -std=c++0x (C++11)
Include the pThreads library (-pthread)


Copy the Exercise_template/src folder to your Eclipse project.

Re-build the project and ensure there are no errors

======================================================================
CMAKE SET-UP
------------

Cmake is a simple utility that builds makefiles. A cmake configuration
file has already been built for this project (CMakeLists.txt).  To use
cmake:

Naviagate to the Exercise_template\build folder

Run cmake using the CMakeLists.txt file in the ..\build folder
to create the project's make files:
$ cd ~\workspace\Two-day C++11\Exercise_template\build
$ cmake ..

Now make the project:
$ make

The output executable (called 'executable' by default) can now be run:
$ ./executable

To rebuild, simply remove all the contents of the build directory and
repeat the above steps:
$ rm -r *
