#ifndef EVENT_H
#define EVENT_H


// ---------------------------------------------------------------------------
// Events represent alarm conditions in the system.
//
class Event
{
  /*************************************************************************
    Modify the Event class as follows:

    - Add an enumeration class attribute that categorises the alarm type; 
      either WARNING, CAUTION or ADVISORY.
    
    - Add a method, type() that returns the event type as an enumeration.

    - Optional - re-work the type() function so that it returns the event
      type as a C-style string (const char*)

  *************************************************************************/
};

#endif // EVENT_H

