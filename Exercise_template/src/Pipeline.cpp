#include "Pipeline.h"
#include "Filter.h"
#include <iostream>

using namespace std;

Pipeline::Pipeline()
{
  filters.reserve(3);
}

void Pipeline::add(IFilter& filter)
{
  filters.push_back(&filter);
}

void Pipeline::run()
{
  // Run the pipeline 10 times
  //
  for (int i = 0; i < 10; ++i)
  {
    for(auto& filter : filters) filter->execute();
    cout << endl;
  }
}
